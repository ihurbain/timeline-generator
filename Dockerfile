FROM php:8.0-apache
COPY index.php /var/www/html/
COPY src/ /var/www/html/src
COPY vendor/ /var/www/html/vendor
RUN mkdir /var/www/html/cache
EXPOSE 80
