<?php

/**
 * @license GPL-2.0-or-later
 * @author Isabelle Hurbain-Palatin
 */

namespace TimelineGenerator;

use TimelineGenerator\Data\Timeline;

require_once __DIR__ . '/../../vendor/autoload.php';

class TimelineGenerator {
	/** @var GraphFetcher */
	private GraphFetcher $gf;

	/** @var TimedataFetcher */
	private TimedataFetcher $tdf;

	/** Default constructor */
	public function __construct() {
		$this->tdf = new TimedataFetcher();
		$this->gf = new GraphFetcher();
	}

	/**
	 * Creates a timeline starting from a Wikipedia article title
	 * @param string $title
	 * @return Timeline
	 */
	public function createTimelineData( string $title ): Timeline {
		$qids = $this->gf->fetchAssociatedWikidataQids( $title );
		$titleQid = $qids[array_key_last( $qids ) ];
		$data = $this->tdf->fetchTimeData( $qids );

		$data = array_merge_recursive( $data,
			$this->tdf->fetchSignificantEvents( $titleQid[0] ) );

		$qidTitleMap = [];
		foreach ( $qids as $qid ) {
			$qidTitleMap[$qid[0]] = $qid[1];
		}
		$timeline = new Timeline( $data, $qidTitleMap );
		//$timeline = $timeline->filterRelevant( $titleQid );
		return $timeline;
	}

}
