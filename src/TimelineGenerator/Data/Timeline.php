<?php

/**
 * @license GPL-2.0-or-later
 * @author Isabelle Hurbain-Palatin
 * @author Subbu Sastry
 */

namespace TimelineGenerator\Data;

class Timeline {
	/** @var array Points of the timeline */
	private array $points;
	private const BATCH_SIZE = 50;

	/**
	 * @param array $data data generated in TimelineGenerator::createTimelineData by
	 * TimedataFetcher::fetchTimeData and TimedataFetcher::fetchSignificantEvents
	 * @param array $qidTitleMap associative array between QID (key) and article title
	 */
	public function __construct( array $data, array $qidTitleMap ) {
		$this->points = [];
		foreach ( $data['results']['bindings'] ?? [] as $item ) {
			if ( isset( $item['itemLabel'] ) ) {
				$this->points[] = PointInTime::createFromDirectData( $item, $qidTitleMap );
			} else {
				if ( isset( $item['pointValue'] ) ) {
					$this->points[] = PointInTime::createFromQualifier( $item, 'point', $qidTitleMap );
				}
				if ( isset( $item['startValue'] ) ) {
					$this->points[] = PointInTime::createFromQualifier( $item, 'start', $qidTitleMap );
				}
				if ( isset( $item['endValue'] ) ) {
					$this->points[] = PointInTime::createFromQualifier( $item, 'end', $qidTitleMap );
				}
			}
		}
		usort( $this->points, [ PointInTime::class, 'compare' ] );
	}

	/**
	 * Applies a few heuristics to filter out the timeline to get rid of irrelevant events.
	 * Current heuristics:
	 *   * keep everything with 20 years (past and future)
	 *   * keep elements linked on at the Wikidata level
	 * TODO improve heuristics, make them configurable on the interface level
	 * @param array $qidTitle
	 * @return $this
	 */
	public function filterRelevant( array $qidTitle ): Timeline {
		$year = $this->estimateEventDate( $qidTitle );
		if ( $year === null ) {
			return $this;
		}
		$tl = new Timeline( [], [] );
		$candidates = [];

		foreach ( $this->points as $point ) {
			if ( abs( $point->getYear() - $year ) < 20 ) {
				$tl->points[] = $point;
			} else {
				$candidates[] = $point;
			}
		}

		$candidatesQid = array_map( fn( $p ) => 'wd:' . $p->getQid(), $candidates );
		$wdQidTitle = 'wd:' . $qidTitle[0];

		$endPoint = 'https://query.wikidata.org/sparql';
		$data = [];

		for ( $i = 0; $i < count( $candidatesQid ); $i += self::BATCH_SIZE ) {
			$qids = implode( ' ', array_slice( $candidatesQid, $i, self::BATCH_SIZE ) );

			$sparql = "select ?item where {
  VALUES ?item {
	$qids
   } .
  { $wdQidTitle ?rel ?item . }
  UNION
  { ?item ?rel $wdQidTitle } .
}";

			$params = [
				'query' => $sparql,
				'format' => 'json',
			];

			$url = $endPoint . "?" . http_build_query( $params );

			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_USERAGENT,
				'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator; ' .
					'ihurbainpalatin@wikimedia.org)' );
			$output = curl_exec( $ch );
			curl_close( $ch );

			$data = array_merge_recursive( $data, json_decode( $output, true ) );
		}

		$keep = [];
		foreach ( $data['results']['bindings'] ?? [] as $d ) {
			$keep[] = str_replace( 'http://www.wikidata.org/entity/', '', $d['item']['value'] );
		}

		$keep = array_unique( $keep );

		foreach ( $this->points as $point ) {
			if ( in_array( $point->getQid(), $keep ) ) {
				$tl->points[] = $point;
			}
		}

		usort( $tl->points, [ PointInTime::class, 'compare' ] );
		return $tl;
	}

	/**
	 * Estimate the year of an event. If a single date is associated to the event, we use that;
	 * if not, we use the average of all these years.
	 * TODO this probably works well enough for contemporary-ish dates with at least year-precision,
	 * 		and probably not at all for stuff like geological periods
	 * @param array $qidTitle
	 * @return int|null
	 */
	private function estimateEventDate( array $qidTitle ): int|null {
		$qid = $qidTitle[0];
		$qidPoints = array_filter( $this->points,
			fn( $pn )  => $pn->getQid() === $qid
		);
		if ( count( $qidPoints ) === 0 ) {
			return null;
		}
		if ( count( $qidPoints ) === 1 ) {
			return $qidPoints[0]->getYear();
		} else {
			$years = [];
			foreach ( $qidPoints as $qidPoint ) {
				$years[] = $qidPoint->getYear();
			}
			return (int)( round( array_sum( $years ) / count( $years ) ) );
		}
	}

	/**
	 * Gets a text representation of a timeline, one event per line
	 * @return string
	 */
	public function getTextRepresentation(): string {
		return implode( PHP_EOL, array_map(
			fn( PointInTime $p ): string => $p->getTextRepresentation(),
			$this->points
		) );
	}

	/**
	 * Gets a Javascript representation of a timeline to feed into a JS UI
	 * @return string
	 */
	public function getJsRepresentation(): string {
		return implode( ',' . PHP_EOL, array_map(
			fn( PointInTime $p ): string => $p->getJsRepresentation(),
			$this->points
		) );
	}
}
