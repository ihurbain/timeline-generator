<?php

/**
 * @license GPL-2.0-or-later
 * @author Isabelle Hurbain-Palatin
 */

namespace TimelineGenerator;

class TimedataFetcher {

	private const BATCH_SIZE = 100;

	/** @var string List of "time related" properties, as established with the use of the
	 * 				TimeProperties script, to which we add P570 (not in that list)
	 */
	private string $TIME_PROPERTIES = 'p:P569 p:570 p:P571 p:P574 p:P575 p:P576 p:P577 p:P580
p:P582 p:P585 p:P606 p:P619 p:P620 p:P621 p:P622 p:P729 p:P730 p:P746 p:P813 p:P1191 p:P1249 p:P1319
p:P1619 p:P1734 p:P2285 p:P2669 p:P2913 p:P3893 p:P3999 p:P6949 p:P7588 p:P7589 p:P8556 p:P9052 p:P9448
p:P9667 p:P10135';

	/**
	 * Fetches "time-like properties" of all the items keyed by the qids
	 * @param array $qids
	 * @return array
	 */
	public function fetchTimeData( array $qids ): array {
		$endPoint = 'https://query.wikidata.org/sparql';

		$itemList = array_map( fn( $s ): string => 'wd:' . $s[0], $qids );

		$data = [];

		for ( $i = 0; $i < count( $itemList ); $i += self::BATCH_SIZE ) {
			$items = implode( ' ', array_slice( $itemList, $i, self::BATCH_SIZE ) );
			$sparql = "SELECT DISTINCT ?item ?itemLabel ?statementValueLabel ?precision ?timeValue WHERE { 
  VALUES ?prop {
	$this->TIME_PROPERTIES
  }
  VALUES ?item { 
	$items
  } . 
  ?item ?prop ?statement .
  ?statement ?rel ?wdv .
  ?wdv wikibase:timeValue ?timeValue .
  ?wdv wikibase:timePrecision ?precision .
  ?statementValue wikibase:statementValue ?rel .
  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }
}";

			$params = [
				'query' => $sparql,
				'format' => 'json',
			];

			$url = $endPoint . "?" . http_build_query( $params );

			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_USERAGENT,
				'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator;' .
				'ihurbainpalatin@wikimedia.org)' );
			$output = curl_exec( $ch );
			curl_close( $ch );

			$data = array_merge_recursive( $data, json_decode( $output, true ) );
		}

		return $data;
	}

	/**
	 * Fetches "significant events" associated to an item keyed by $qid
	 * @param string $qid
	 * @return array
	 */
	public function fetchSignificantEvents( string $qid ): array {
		$endPoint = 'https://query.wikidata.org/sparql';

		$sparql = "SELECT DISTINCT ?qid ?qidLabel ?pointValue ?pointPrec
		?startValue ?startPrec?endValue ?endPrec ?eventLabel WHERE {
  VALUES ?qid {
    wd:$qid
  }
  ?qid p:P793 ?s1 .
  OPTIONAL{ ?s1 pqv:P585/wikibase:timeValue ?pointValue. }
  OPTIONAL{ ?s1 pqv:P585/wikibase:timePrecision ?pointPrec. }
  OPTIONAL{ ?s1 pqv:P580/wikibase:timeValue ?startValue . }
  OPTIONAL{ ?s1 pqv:P580/wikibase:timePrecision ?startPrec. }
  OPTIONAL{ ?s1 pqv:P582/wikibase:timeValue ?endValue . }
  OPTIONAL{ ?s1 pqv:P582/wikibase:timePrecision ?endPrec. }
  ?s1 (pq:P585|pq:P580|pq:P582) ?qual .
  ?s1 ps:P793 ?event .
  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }
}";

		$params = [
			'query' => $sparql,
			'format' => 'json',
		];

		$url = $endPoint . "?" . http_build_query( $params );

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT,
			'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator; ' .
			'ihurbainpalatin@wikimedia.org)' );
		$output = curl_exec( $ch );
		curl_close( $ch );
		$data = json_decode( $output, true );

		return $data ?? [];
	}
}
