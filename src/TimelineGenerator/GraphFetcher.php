<?php

/**
 * @license GPL-2.0-or-later
 * @author Isabelle Hurbain-Palatin
 */

namespace TimelineGenerator;

class GraphFetcher {

	/**
	 * Helper function to fetch links on enwiki, including result batch handling.
	 * TODO have enpoint as option
	 * @param array $queryParams
	 * @param array $continueParams
	 * @return array
	 */
	private function fetchLinks( array $queryParams, array $continueParams ): array {
		$endPoint = 'https://en.wikipedia.org/w/api.php';
		$params = array_merge(
			[
				'action' => 'query',
				'format' => 'json',
			], $queryParams
		);

		$continue = '';
		$continuePoint = '';
		$result = [];

		do {
			if ( $continue === $continueParams['indicator'] ) {
				$params[ 'continue' ] = $continue;
				$params[ $continueParams[ 'point' ] ] = $continuePoint;
			}

			$url = $endPoint . "?" . http_build_query( $params );

			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_USERAGENT,
				'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator;' .
				'ihurbainpalatin@wikimedia.org)' );
			$output = curl_exec( $ch );
			curl_close( $ch );

			$tmpResult = json_decode( $output, true );
			$continue = $tmpResult[ 'continue' ][ 'continue' ] ?? '';
			$continuePoint = $tmpResult[ 'continue' ][ $continueParams['point'] ] ?? '';
			$result = array_merge_recursive( $result, $tmpResult );
		} while ( $continue === $continueParams[ 'indicator' ] );

		return $result;
	}

	/**
	 * Get all outgoing links of a page
	 * TODO handle redirects
	 * @param string $title
	 * @return array
	 */
	public function fetchOutgoingLinks( string $title ): array {
		$params = [
			'titles' => $title,
			'prop' => 'links',
			'pllimit' => 'max',
			'plnamespace' => '0',
		];
		$continueParams = [ 'indicator' => '||', 'point' => 'plcontinue' ];
		$result = $this->fetchLinks( $params, $continueParams );

		$titles = [];

		foreach ( $result['query']['pages'] as $res ) {
			foreach ( $res['links'] as $v ) {
				$titles[] = $v['title'];
			}
		}
		return $titles;
	}

	/**
	 * Get all incoming links to a page
	 * @param string $title
	 * @return array
	 */
	public function fetchIncomingLinks( string $title ): array {
		$params = [
			'list' => 'backlinks',
			'bltitle' => $title,
			'bllimit' => 'max',
			'blnamespace' => '0',
			'blredirect' => true
		];

		$continueParams = [ 'indicator' => '-||', 'point' => 'blcontinue' ];

		$result = $this->fetchLinks( $params, $continueParams );

		$titles = [];
		foreach ( $result['query']['backlinks'] as $res ) {
			$titles[] = $res['title'];
		}
		return $titles;
	}

	/**
	 * Get all the QID keying the wikidata of the incoming and outgoing links of the page whose
	 * title is given as parameter
	 * TODO give enwiki/en as options
	 * @param string $title
	 * @return array
	 */
	public function fetchAssociatedWikidataQids( string $title ): array {
		$titles = $this->fetchIncomingLinks( $title );
		$titles = array_merge( $titles, $this->fetchOutgoingLinks( $title ) );
		$titles[] = $title;
		$titles = array_unique( $titles );
		$endPoint = 'https://www.wikidata.org/w/api.php';
		$params = [
			'action' => 'wbgetentities',
			'format' => 'json',
			'sites' => 'enwiki',
			'titles' => implode( '|', array_slice( $titles, 0, 50 ) ),
			'props' => 'info|sitelinks',
			'languages' => 'en',
		];

		$qids = [];

		for ( $i = 0; $i < count( $titles ); $i += 50 ) {
			$params['titles'] = implode( '|', array_slice( $titles, $i, 50 ) );
			$url = $endPoint . "?" . http_build_query( $params );

			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_USERAGENT,
				'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator;' .
				'ihurbainpalatin@wikimedia.org)' );
			$output = curl_exec( $ch );
			curl_close( $ch );

			$result = json_decode( $output, true );
			foreach ( $result[ 'entities' ] ?? [] as $k => $v ) {
				if ( str_starts_with( $k, 'Q' ) ) {
					$qids[] = [ $k, $v[ 'sitelinks' ][ 'enwiki' ][ 'title' ] ];
				}
			}
		}
		return $qids;
	}
}
