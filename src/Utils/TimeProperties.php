<?php

/**
 * @license GPL-2.0-or-later
 * @author Isabelle Hurbain-Palatin
 */

namespace Utils;

class TimeProperties {
	public function fetchTimeProperties(): array {
		$endPoint = 'https://query.wikidata.org/sparql';

		// Fetch all the categories that are instances of Q18636219: "Wikidata property with datatype 'time'"
		// TODO this is probably not the right query. Good enough for now, but how do I actually
		//		get that information?
		$sparql = 'SELECT ?propid WHERE { ?item wdt:P31 wd:Q18636219. ' .
			'bind(replace(str(?item), "http://www.wikidata.org/entity/", "") as ?propid) }';

		$params = [
			'query' => $sparql,
			'format' => 'json',
		];

		$url = $endPoint . "?" . http_build_query( $params );

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT,
			'TimelineGenerator/0.1 (https://gitlab.wikimedia.org/ihurbain/timeline-generator;' .
			'ihurbainpalatin@wikimedia.org)' );
		$output = curl_exec( $ch );
		curl_close( $ch );

		$data = json_decode( $output, true );

		$propids = [];
		foreach ( $data['results']['bindings'] ?? [] as $v ) {
			$propids[] = $v['propid']['value'];
		}
		return $propids;
	}
}

$tp = new TimeProperties();
$props = $tp->fetchTimeProperties();
foreach ( $props as $prop ) {
	echo 'p:' . $prop . ' ';
}
echo "\n";
