<?php

require_once __DIR__ . '/vendor/autoload.php';

use TimelineGenerator\TimelineGenerator;

$tlg = new TimelineGenerator();

$title = $_GET['title'] ?? null;


if ( $title === null ) {
    echo '<form action="/index.php" method="get"><input type="text" name="title" /><input type="submit"></form>';
} else {
    if ( $_GET['nofetch'] ?? false ) {
        $timeline = unserialize( file_get_contents( __DIR__ . '/cache/' . md5( $title ) ) );
    } else {
        $timeline = $tlg->createTimelineData($title);
    }
    if ( $_GET['cache'] ?? false ) {
        // md5 on the title ensures that we don't have weird chars in the title name
        // probably lazy, good enough for now anyway
        file_put_contents( __DIR__ . '/cache/' . md5( $title ), serialize( $timeline ) );
    }
    //$tl = unserialize( $ser );
    echo $timeline->getTextRepresentation();
}
