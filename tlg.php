<?php

require_once __DIR__ . '/vendor/autoload.php';

use TimelineGenerator\TimelineGenerator;

$tlg = new TimelineGenerator();
$title = $argv[1] ?? 'The Long Way to a Small, Angry Planet';
$timeline = $tlg->createTimelineData( $title );
echo $timeline->getTextRepresentation() . PHP_EOL;