# Timeline Generator

Generate timelines automatically from Wikipedia link graph & Wikidata.

## Warning

The provided Docker/docker compose scripts are strictly for development/debugging purposes. Do not deploy on a publicly accessible machine!!

Contact: Isabelle Hurbain-Palatin <ihurbainpalatin@wikimedia.org>
